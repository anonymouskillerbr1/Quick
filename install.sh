#!/bin/bash
clear
echo "
██████╗ ██████╗ ██╗   ██╗ ██████╗███████╗
██╔══██╗██╔══██╗╚██╗ ██╔╝██╔════╝██╔════╝
██████╔╝██████╔╝ ╚████╔╝ ██║     █████╗
██╔═══╝ ██╔══██╗  ╚██╔╝  ██║     ██╔══╝
██║     ██║  ██║   ██║   ╚██████╗███████╗
╚═╝     ╚═╝  ╚═╝   ╚═╝    ╚═════╝╚══════╝

▀▀█▀▀ █▀▀█ █▀▀█ █   █▀▀  Instalador de Ferramentas Anonymouskillerbr1
  █   █  █ █  █ █   ▀▀█
  ▀   ▀▀▀▀ ▀▀▀▀ ▀▀▀ ▀▀▀

                                                ";

echo "[✔] Verificar diretorios...";
if [ -d "/usr/share/doc/Quick" ] ;
then
echo "[◉] Um diretorio Quick foi encontrado! Quer substituir? [Y/n]:" ; 
read mama
if [ $mama == "y" ] ; 
then
 rm -R "/usr/share/doc/Quick"
else
 exit
fi
fi

 echo "[✔] Instalando ...";
 echo "";
 git clone https://github.com/anonymouskillerbr1/Quick.git /usr/share/doc/Quick;
 echo "#!/bin/bash 
 python /usr/share/doc/Quick/quick.py" '${1+"$@"}' > quick;
 chmod +x quick;
 sudo cp quick /usr/bin/;
 rm quick;


if [ -d "/usr/share/doc/Quick" ] ;
then
echo "";
echo "[✔]Ferramenta instalada com sucesso![✔]";
echo "";
  echo "[✔]====================================================================[✔]";
  echo "[✔] Tudo pronto!! Voce pode executar a ferramenta digitando ./quick.py [✔]"; 
  echo "[✔]====================================================================[✔]";
  echo "";
else
  echo "[✘] Instalacao falhada![✘] ";
  exit
fi
